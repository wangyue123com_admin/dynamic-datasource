package com.example.service;

import com.example.dao.BaseSystemMapper;
import com.example.pojo.BaseSystem;
import com.example.pojo.BaseSystemExample;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * @Author wangy
 * @create 2021/7/12 14:11
 * @Description
 */
@Service
public class SystemService {

    @Resource
    private BaseSystemMapper baseSystemMapper;

    public List<BaseSystem> getSystemInfo(){
        BaseSystemExample le = new BaseSystemExample();
        List<BaseSystem> baseSystems = baseSystemMapper.selectByExample(le);
        return baseSystems;
    }
}
