package com.example.controller;

import com.example.pojo.BaseSystem;
import com.example.service.SystemService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;

/**
 * @Author wangy
 * @create 2021/7/12 14:14
 * @Description 获取当前租户的系统信息
 */
@Controller
@RequestMapping("/system")
public class BaseSystemController {
    @Autowired
    private SystemService systemService;

    @ResponseBody
    @GetMapping("/getSystemInfo")
    public List<BaseSystem> getSystemInfo(){
        return systemService.getSystemInfo();
    }

}
