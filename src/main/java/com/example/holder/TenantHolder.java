package com.example.holder;

/**
 * @Author wangy
 * @create 2021/7/16 9:41
 * @Description  ThreadLocal来保存当前租户的标识，从而获取租户的数据源
 */
public class TenantHolder {

    //通过拦截器字符串截取 host.split("\\.")[0] 来保存当前租户的标识
    private static ThreadLocal<String> tenantCode = new ThreadLocal<>();

    public static String getTenantCode() {
        return tenantCode.get();
    }

    public static void setTenantCode(String code) {
        tenantCode.set(code);
    }

    public static void remove(){
        tenantCode.remove();
    }

}
