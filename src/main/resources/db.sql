

create
database defaultDB character set utf8;
use
defaultDB;

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for t_tenant
-- ----------------------------
DROP TABLE IF EXISTS `t_tenant`;
CREATE TABLE `t_tenant`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `tenant_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `tenant_code` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `db_url` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `db_user` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `db_password` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `tenant_introduce` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 7 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of t_tenant
-- ----------------------------
INSERT INTO `t_tenant` VALUES (5, '张三的系统信息', 'zhangsan', 'jdbc:mysql://localhost:3306/zhangsan?useUnicode=true&characterEncoding=utf-8&serverTimezone=GMT%2B8', 'root', '123456', '租户张三');
INSERT INTO `t_tenant` VALUES (6, '李四的系统信息', 'lisi', 'jdbc:mysql://localhost:3306/lisi?useUnicode=true&characterEncoding=utf-8&serverTimezone=GMT%2B8', 'root', '123456', '租户李四');

SET FOREIGN_KEY_CHECKS = 1;


create
database lisi character set utf8;
use
lisi;

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for base_system
-- ----------------------------
DROP TABLE IF EXISTS `base_system`;
CREATE TABLE `base_system`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `key_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '系统配置名',
  `key_value` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '系统配置值',
  `add_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `update_time` datetime(0) NULL DEFAULT NULL COMMENT '更新时间',
  `deleted` tinyint(1) NULL DEFAULT 0 COMMENT '逻辑删除',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 15 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of base_system
-- ----------------------------
INSERT INTO `base_system` VALUES (1, '李四的数据源', '李四', '2021-07-16 14:40:16', '2021-07-16 14:40:19', 0);

SET FOREIGN_KEY_CHECKS = 1;



create
database zhangsan character set utf8;
use
zhangsan;

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for base_system
-- ----------------------------
DROP TABLE IF EXISTS `base_system`;
CREATE TABLE `base_system`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `key_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '系统配置名',
  `key_value` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '系统配置值',
  `add_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `update_time` datetime(0) NULL DEFAULT NULL COMMENT '更新时间',
  `deleted` tinyint(1) NULL DEFAULT 0 COMMENT '逻辑删除',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 15 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of base_system
-- ----------------------------
INSERT INTO `base_system` VALUES (1, '张三的的数据源', '张三', '2021-07-12 15:58:14', '2021-07-12 15:58:16', 0);

SET FOREIGN_KEY_CHECKS = 1;